"""
Problem Overview: Network Failure Point
We have a mesh network connected by routers labeled from 1 to 6 in a directed manner.
Write an algorithm that can detect the routers with the highest number of connections so we might know which routers will cause a network failure if removed.
Multiple connections between the same routers should be treated as separate connections. A tie for the most number of connections is possible.

Requirements
Implement a identify_router function that accepts an input graph of nodes representing the total network and identifies the node with the most number of connections.
Return the label of the node.
Implement a directed graph data structure using Python 3.6 and up.
Each node is unique thus there will be no cases of having multiple nodes with the same label.
Each node will have an infinite number of both inbound and outbound links.

Test Cases

1 -> 2 -> 3 -> 5 -> 2 -> 1 = 2 *since router 2 has 2 inbound links and 2 outbound links

1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6 = 5 * since router 5 has 2 inbound links and 2 outbound link

2 -> 4 -> 6 -> 2 -> 5 -> 6 = 2, 6 * since router 2 has 1 inbound link and 2 outbound links and 6 has 2 inbound links and 1 outbound link
Explanation
Explain time complexity of the identify_router function written.

"""

# Node in graph
# Each node has name, their inbound and outbound connection values, their children/neighbours


class Node:
    def __init__(self, name):
        self.name = name
        self.inbound = 0
        self.outbound = 0

        self.children = set()

    def add_children(self, v):  # add neighbour/children for a node
        if v not in self.children:
            self.children.add(v)


class Graph:
    def __init__(self, total_node):
        """
        initiate the graph 
        """

        self.total_nodes = total_node
        self.vertices = {}

        for i in range(1, self.total_nodes+1):  # initialize the graph adding all vertex
            node = Node(i)
            self.vertices[node.name] = node

    def add_edges(self, u, v):
        """
        add edges between two vertices, 
        keep track of incoming and outgoing nodes
        u -> from node
        v -> to node
        """
        if u in self.vertices and v in self.vertices:
            for key, val in self.vertices.items():
                if key == u:
                    val.add_children(v)
                    val.outbound += 1
                elif key == v:
                    val.inbound += 1
        else:
            print("Wrong Vertex Number")

    def print_graph(self):
        """
        print the graph as adjacency list
        """
        for key in self.vertices.keys():
            print(key, self.vertices[key].children)


def identify_router(graph):
    """
    identify the node/router which has maximum connections,
    if tie, add all
    """
    # dictionary to keep track of nodes by connection number
    ans = {}
    for key in graph.vertices.keys():
        # accumalition all connections for a particular node
        connections = graph.vertices[key].inbound + \
            graph.vertices[key].outbound

        if connections not in ans:
            ans[connections] = [key]

        else:
            ans[connections].append(key)

    failure_point = -float("Inf")

    # loop to find out maximum connection and it's associated nodes
    for key in ans.keys():
        if key > failure_point:
            failure_point = key

    # return the nodes of maximum connections
    return ans[failure_point]


# initiating graph with the number of vertex
network = Graph(6)

# adding edges as (from,to) format
network.add_edges(2, 4)
network.add_edges(4, 6)
network.add_edges(6, 2)
network.add_edges(2, 5)
network.add_edges(5, 6)


assert identify_router(network) == [2, 6]


"""
Complexity:
Runtime complexity: O(n), where n is the number of vertices

Space complexity : O(n), where n is to store the connection numbers
"""
