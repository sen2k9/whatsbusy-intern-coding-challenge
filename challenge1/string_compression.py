"""
Challenge 1: Algorithm Test

Problem Overview: String Compression
Implement a string compression using python. For example, aaaabbbccddddddee would become a4b3c2d6e2. If the length of the string is not reduced, return the original string.

Requirements
Implement a compress function which accepts an input string and returns a compressed string. Code must be implemented using python 3.6 and must follow strictly pep8 rules.

Provide comments regarding the implementation.

Test Cases
assert compress(‘bbcceeee’) == ‘b2c2e4’
assert compress(‘aaabbbcccaaa’) == ‘a3b3c3a3’
assert compress(‘a’) = a
Explanation
Explain time complexity of the compression written.

"""


def compress(string):
    """compressed a string
    :type string : str
    :rtype : str
    """
    # if the string is empty
    if not string:
        return string

    j = 0  # pointer -> to point evey letter in the string
    prev = string[j]  # first letter in the string
    ans = []  # save the answer in a list
    count = 1  # start with count = 1

    while j < len(string)-1:  # iterate the pointer until one letter left

        j += 1  # increment the pointer in every loop

        if string[j] == prev:  # if the present letter is same as the previous increment the count
            count += 1
        else:  # if the present letter is different than the previous, then save the previous results
            ans.append(prev)
            ans.append(str(count))
            prev = string[j]  # point prev variable to new letter
            count = 1  # start count = 1 for new leter

    # loop finishes before saving the last letter and it's count, save it after the loop
    ans = ans + [prev, str(count)]

    # if the compressed string is larger than the original return the original, else return the compressed string
    if len(ans) > len(string):
        return string
    else:
        # join all the letter from the list ans to a string
        return "".join(ans)


assert compress("bbcceeee") == "b2c2e4"
assert compress("aaabbbcccaaa") == "a3b3c3a3"
assert compress("a") == "a"
assert compress("") == ""
assert compress("abcdef") == "abcdef"

"""
complexity analysis:
Runtime complexity: O(n), where n is the length of the string
Space complexity : O(n), answer saves to a list
"""
